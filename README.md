# Xcode 4 color theme
#### based on Solarized light theme and EMACS Adwaita + some hoodoo =)
Used third party font (in ZIP)
***
## Preview
 ![Screenshot](https://bitbucket.org/Kamiel/adwaita-xcode4/raw/20c51ae75497791eb70de90b87d1f47a0783bb36/Adwaita%20theme%20preview.png)
***
## Installation
 * install fonts from ZIP (optional)
 * copy theme into ~/Library/Developer/Xcode/UserData/FontAndColorThemes
 * restart Xcode
 * Xcode->Preferences...->Fonts&Colors -- select Adwaita theme in list on the left
***
## References
### coding fonts
- [Inconsolata](http://levien.com/type/myfonts/inconsolata.html)
- [Ubuntu Mono](http://font.ubuntu.com)
- [Droid Mono Slashed](http://blog.cosmix.org/2009/10/27/a-slashed-zero-droid-sans-mono/)
- [Droid Mono Dotted](http://blog.cosmix.org/2009/01/25/a-dotted-zero-droid-sans-mono/)
